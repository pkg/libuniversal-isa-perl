libuniversal-isa-perl (1.20171012-1.1co0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 28 Apr 2021 16:45:54 +0200

libuniversal-isa-perl (1.20171012-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 28 Dec 2020 13:46:05 +0100

libuniversal-isa-perl (1.20171012-1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Nathan Handler from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Florian Schlichting ]
  * Import upstream version 1.20171012
  * Update upstream metadata
  * Declare compliance with Debian Policy 4.1.1

 -- Florian Schlichting <fsfs@debian.org>  Mon, 13 Nov 2017 21:34:45 +0100

libuniversal-isa-perl (1.20150614-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.20150614
  * Update build dependencies
  * Set 9 level in d/compat

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Fri, 14 Aug 2015 14:30:07 -0300

libuniversal-isa-perl (1.20140927-1) unstable; urgency=medium

  * Imported upstream version 1.20140927
  * Update debian/copyright.
    New Upstream-Contact, bump packaging copyright years.
  * Install CONTRIBUTING document.
  * Explicitly build-depend on libcgi-pm-perl.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Fri, 10 Oct 2014 15:30:01 +0200

libuniversal-isa-perl (1.20140824-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Imported Upstream version 1.20140824
  * adjust years of copyright following upstream
  * Add debian/upstream/metadata
  * build-depend on libmodule-build-tiny-perl and bump debhelper dependency
  * Standards-Version: 3.9.5 (no changes needed)

 -- Damyan Ivanov <dmn@debian.org>  Tue, 26 Aug 2014 13:32:01 +0000

libuniversal-isa-perl (1.20120726-1) unstable; urgency=low

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
    Fixes "FTBFS with perl 5.18: test failure"
    (Closes: #711276)
  * Update years of packaging copyright.
  * Set Standards-Version to 3.9.4 (no further changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 08 Jun 2013 00:34:45 +0200

libuniversal-isa-perl (1.20120418-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Alessandro Ghedini ]
  * Email change: Alessandro Ghedini -> ghedo@debian.org

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: update to Copyright-Format 1.0.
  * Add /me to Uploaders.
  * Bump Standards-Version to 3.9.3 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Mon, 23 Apr 2012 16:04:06 +0200

libuniversal-isa-perl (1.20110614-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * New upstream release 1.03
  * Add myself to Uploaders
  * Debian Policy 3.8.2
  * Update ryan52's email address

  [ gregor herrmann ]
  * Add perl (>= 5.10.1) as an alternative build dependency for
    Module::Build.

  [ Alessandro Ghedini ]
  * New upstream release
  * Switch to 3.0 (quilt) format
  * Bump debhelper compat level to 8
  * Remove libmodule-build-perl from Build-Depends (no more used)
  * Bump Standards-Version to 3.9.2
    - Remove versioned Build-Depends-Indep on perl
  * Add myself to Uploaders
  * Remove trailing comma from Depends field
  * Remove non-copyright holders from debian/copyright
  * Update upstream license and copyright
    - Update upstream copyright years
    - License is now Artistic or GPL-1+, change debian/* licensing accordingly
      (see the comment in debian/copyright for debian/* licensing)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Wed, 15 Jun 2011 19:25:39 +0200

libuniversal-isa-perl (1.02-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Nathan Handler ]
  * New upstream release
  * debian/control:
    - Bump Standards-Vesion to 3.8.1 (No changes)
    - Add myself to list of Uploaders
  * debian/copyright:
    - Switch to new copyright format

  [ gregor herrmann ]
  * debian/control: make build dependency on libmodule-build-perl versioned.
  * Minimize debian/rules.

 -- Nathan Handler <nhandler@ubuntu.com>  Mon, 01 Jun 2009 00:58:34 +0000

libuniversal-isa-perl (1.01-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * Refresh debian/rules, no functional changes; except: don't create
    .packlist file any more.
  * debian/watch: use dist-based URL.
  * Set Standards-Version to 3.8.0 (no changes).
  * Slightly improve short description.
  * debian/copyright: use author-independent upstream source URL; add actual
    copyright statement; wrap a long line.

  [ Damyan Ivanov ]
  * New upstream release
  * now uses Artistic 2.0 license only. Update debian/copyright; While there,
    refresh the years of copyright too
  * debian/rules: rewrite with debhelper 7; bump compat and dependency
    - do not install redundant README
  * add myself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Wed, 27 Aug 2008 15:25:34 +0300

libuniversal-isa-perl (0.06-1) unstable; urgency=low

  * Initial Release (closes: #365142).

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 27 Apr 2006 15:52:30 +0200

